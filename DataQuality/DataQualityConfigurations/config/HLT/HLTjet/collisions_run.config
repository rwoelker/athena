######################################################################
# $Id: collisions_run.config Sun Mar 27 16:21:21 2022 ayana $
######################################################################

#######################
# HLTjet
#######################

reference HLTJetRef {
  location = /eos/atlas/atlascerngroupdisk/data-dqm/references/Collisions/,root://eosatlas.cern.ch//eos/atlas/atlascerngroupdisk/data-dqm/references/
  file = data15_13TeV.00267638.physics_EnhancedBias.merge.HIST.r6857_p1831.root
  path = run_267638
  name = same_name
}

algorithm HLTjetSimpleSummary {
  libname = libdqm_summaries.so
  name = SimpleSummary
}

##
compositeAlgorithm HLTjet_Histogram_Not_Empty&GatherData {
  subalgs = GatherData,Histogram_Not_Empty
  libnames = libdqm_algorithms.so
}

algorithm HLTjet_Histogram_Not_Empty&GatherData {
  name = HLTjet_Histogram_Not_Empty&GatherData
}

algorithm HLTjet_Histogram_Not_Empty_with_Ref&GatherData {
  name = HLTjet_Histogram_Not_Empty&GatherData
  #reference = HLTJetRef
  reference = stream=physics_Main:CentrallyManagedReferences_TriggerMain;CentrallyManagedReferences_Trigger 
}

##

compositeAlgorithm HLTjet_GatherData&Chi2Test {
  subalgs = GatherData,Chi2Test_Chi2_per_NDF
  libnames = libdqm_algorithms.so
}

algorithm HLTjet_Chi2NDF {
  name = HLTjet_GatherData&Chi2Test
  thresholds = HLTjet_Chi2_Thresh
  #reference = HLTJetRef
  reference = stream=physics_Main:CentrallyManagedReferences_TriggerMain;CentrallyManagedReferences_Trigger
  MinStat = 1000
}

thresholds HLTjet_Chi2_Thresh {
  limits Chi2_per_NDFt {
    warning = 5
    error = 8
  }
}

##

algorithm HLTjet_KolmogorovTest_MaxDist {
  libname = libdqm_algorithms.so
  name = KolmogorovTest_MaxDist
  thresholds = HLTjet_KolmogorovThresh
  MinStat = 100
  #reference = HLTJetRef
  reference = stream=physics_Main:CentrallyManagedReferences_TriggerMain;CentrallyManagedReferences_Trigger
}

thresholds HLTjet_KolmogorovThresh {
  limits MaxDist {
    warning = 0.05
    error = 0.15
  }
}


thresholds HLTjetEta_BinsDiff_Threshold {
  limits MaxDeviation {
    warning = 3.0
    error = 6.0
  }
}

algorithm HLTjetEtaPhiAve_BinsDiff {
  libname = libdqm_algorithms.so
  name = GatherData&BinsDiffStrips
  PublishBins = 1
  MaxPublish = 10
  SigmaThresh = 0.
  xmin = -3.15
  xmax = 3.15
  MinStat = 15000
  TestConsistencyWithErrors = 0
  thresholds = HLTjetEta_BinsDiff_Threshold
  #reference = HLTJetRef
  reference = stream=physics_Main:CentrallyManagedReferences_Main;CentrallyManagedReferences
}



#######################
# Output
#######################


output top_level {
algorithm = HLTjetSimpleSummary
output HLT {
  output TRJET {
    output Shifter {
      output Online {
        output HLT_AntiKt10EMPFlowCSSKSoftDropBeta100Zcut10Jets_jes_ftf {
	  output NoTriggerSelection {
	  }
	}
	output HLT_AntiKt4EMPFlowJets_subresjesgscIS_ftf {
	  output NoTriggerSelection {
	    output MatchedOffline {
	    }
	  }
	  output HLT_j45_pf_ftf_preselj20_L1J15 {
	  }	  	  	  
	  output HLT_j45_pf_ftf_preselj20_L1jJ40 {
          }
	  output HLT_j60_pf_ftf_preselj50_L1jJ50 {
	  }
	}
	output HLT_AntiKt4EMTopoJets_subjesIS {
	  output NoTriggerSelection {
            output MatchedOffline {
            }
          }
	  output HLT_j45_320eta490_L1J15p31ETA49 {
          }
	  output HLT_j45_320eta490_L1jJ40p31ETA49 {
          }
        }	
      }
      output L1 {
        output LVL1JetRoIs {
	}
	output L1_jFexSRJetRoI {
	}
      }      
    }
    # expert
    output Expert {
      output Online {
        output HLT_AntiKt10EMPFlowCSSKSoftDropBeta100Zcut10Jets_jes_ftf {
          output NoTriggerSelection {
          }
        }
        output HLT_AntiKt10EMPFlowCSSKSoftDropBeta100Zcut10Jets_nojcalib_ftf {
          output NoTriggerSelection {
          }
        }
        output HLT_AntiKt10EMPFlowSoftDropBeta100Zcut10Jets_nojcalib_ftf {
          output NoTriggerSelection {
          }
        }
        output HLT_AntiKt10LCTopoTrimmedPtFrac4SmallR20Jets_jes {
          output NoTriggerSelection {
          }
        }
        output HLT_AntiKt4EMPFlowJets_nojcalib_ftf {
          output NoTriggerSelection {
          }
        }
        output HLT_AntiKt4EMTopoJets_subjesgscIS_ftf {
          output NoTriggerSelection {
          }
        }
        output HLT_AntiKt4EMPFlowJets_subresjesgscIS_ftf {
          output NoTriggerSelection {
          }
          output HLT_noalg_L1jJ80 {
          }
          output HLT_j60_pf_ftf_preselj50_L1jJ50 {
          }
          output HLT_j45_pf_ftf_preselj20_L1jJ40 {
          }
          output HLT_j45_pf_ftf_preselj20_L1J15 {
          }
        }
        output HLT_AntiKt4EMPFlowJets_subjesgscIS_ftf {
          output NoTriggerSelection {
          }
        }
        output HLT_AntiKt4EMTopoJets_subjesIS {
          output NoTriggerSelection {
          }
          output HLT_j45_320eta490_L1jJ40p31ETA49 {
          }
          output HLT_j45_320eta490_L1J15p31ETA49 {
          }
        }
      }
      output Offline {
        output AntiKt4EMTopoJets {
          output standardHistos {
          }
	  output MatchedPFlowJets {
          }          
          output LooseBadFailedJets {
          }
        }
        output AntiKt4EMPFlowJets {
          output standardHistos {
          }
          output LooseBadFailedJets {
          }
        }
      }
      output L1 {
        output L1_jFexSRJetRoI {
          output NoTriggerSelection {
            output MatchedRecoJets {
            }
            output MatchedTrigJets {
            }
          }
          output L1_jJ40 {
          }
          output L1_jJ50 {
          }
        }
        output LVL1JetRoIs {
          output NoTriggerSelection {
            output MatchedRecoJets {
            }
            output MatchedTrigJets {
            }
          }
          output L1_J20 {
          }
          output L1_J15 {
          }
        }
      }
    }
  }
}
}

#######################
# Histogram Assessments
#######################

dir HLT {
  dir JetMon {
    dir Online {
      dir HLT_AntiKt10EMPFlowCSSKSoftDropBeta100Zcut10Jets_jes_ftf {
        description   = LargeR primary reconstruction collection histogram checks
        dir NoTriggerSelection {	  
	  hist [^_]*@shifter  {
	    regex         = 1
            algorithm     = HLTjet_KolmogorovTest_MaxDist
            output        = HLT/TRJET/Shifter/Online/HLT_AntiKt10EMPFlowCSSKSoftDropBeta100Zcut10Jets_jes_ftf/NoTriggerSelection          
	  }
	  # all underscores are expert for large-R. Chi2 for both th1 and th2.
          hist .*[_].*@expert {
            regex         = 1
            algorithm     = HLTjet_Chi2NDF
            output        = HLT/TRJET/Expert/Online/HLT_AntiKt10EMPFlowCSSKSoftDropBeta100Zcut10Jets_jes_ftf/NoTriggerSelection
          }
        }
      }
      dir HLT_AntiKt10EMPFlowCSSKSoftDropBeta100Zcut10Jets_nojcalib_ftf {
        dir NoTriggerSelection {
          hist .*@expert {
            regex         = 1
            algorithm     = HLTjet_Chi2NDF
            description   = Compare to standard AntiKt10EMPFlowCSSKSoftDropBeta100Zcut10Jets to check calibration.
            output        = HLT/TRJET/Expert/Online/HLT_AntiKt10EMPFlowCSSKSoftDropBeta100Zcut10Jets_nojcalib_ftf/NoTriggerSelection
            display       = StatBox
          }
        }
      }
      dir HLT_AntiKt10EMPFlowSoftDropBeta100Zcut10Jets_nojcalib_ftf {
        dir NoTriggerSelection {
          hist .*@expert {
            regex         = 1
            algorithm     = HLTjet_Chi2NDF
            description   = Compare to HLT_AntiKt10EMPFlowCSSKSoftDropBeta100Zcut10Jets_nojcalib_ftf folder to check effect of constituent modification.
            output        = HLT/TRJET/Expert/Online/HLT_AntiKt10EMPFlowSoftDropBeta100Zcut10Jets_nojcalib_ftf/NoTriggerSelection
            display       = StatBox
          }
        }
      }
      dir HLT_AntiKt10LCTopoTrimmedPtFrac4SmallR20Jets_jes {
        dir NoTriggerSelection {
          hist [^_]*@expert {
            regex         = 1
            algorithm     = HLTjet_Chi2NDF
            description   = Compare to reference.
            output        = HLT/TRJET/Expert/Online/HLT_AntiKt10LCTopoTrimmedPtFrac4SmallR20Jets_jes/NoTriggerSelection
            display       = StatBox
          }
        }
      }
      dir HLT_AntiKt4EMPFlowJets_nojcalib_ftf {
        dir NoTriggerSelection {
          hist [^_]*@expert {
            regex         = 1
            algorithm     = HLTjet_Chi2NDF
            description   = Compare to primary calibrated collection to check calibration.
            output        = HLT/TRJET/Expert/Online/HLT_AntiKt4EMPFlowJets_nojcalib_ftf/NoTriggerSelection
            display       = StatBox
          }
        }
      }
      dir HLT_AntiKt4EMTopoJets_subjesgscIS_ftf {
        dir NoTriggerSelection {
          hist [^_]*@expert {
            regex         = 1
            algorithm     = HLTjet_Chi2NDF
            description   = Compare to primary collection to check effect of pileup residual correction.
            output        = HLT/TRJET/Expert/Online/HLT_AntiKt4EMTopoJets_subjesgscIS_ftf/NoTriggerSelection
            display       = StatBox
          }
        }
      }
      dir HLT_AntiKt4EMPFlowJets_subresjesgscIS_ftf {
        dir NoTriggerSelection {
          dir MatchedJets_AntiKt4EMPFlowJets {
            hist [^_]*@shifter {
              regex         = 1
              algorithm     = HLTjet_KolmogorovTest_MaxDist
	      description   = Check for changes in online or offline. 
              output        = HLT/TRJET/Shifter/Online/HLT_AntiKt4EMPFlowJets_subresjesgscIS_ftf/NoTriggerSelection/MatchedOffline
              display       = StatBox
            }
	    hist .*_.*@shifter {
	      regex         = 1
              algorithm     = HLTjet_Chi2NDF
	      description   = Compare offline and online jet reco.  NO AUTOMATIC CHECKS
              output        = HLT/TRJET/Shifter/Online/HLT_AntiKt4EMPFlowJets_subresjesgscIS_ftf/NoTriggerSelection/MatchedOffline
	    }
          }
	  hist eta_phi@shifter {
            algorithm     = HLTjet_Chi2NDF
	    description   = Check for true hotspots (not low-stats artefacts) in EMPFlow jet distribution
	    output	  = HLT/TRJET/Shifter/Online/HLT_AntiKt4EMPFlowJets_subresjesgscIS_ftf/NoTriggerSelection
	  }
	  hist phi_e@shifter {
	    algorithm     = HLTjet_Chi2NDF  
	    description   = Check for uniformity in phi.  AUTOMATIC TEST DISABLED!
	    output	  = HLT/TRJET/Shifter/Online/HLT_AntiKt4EMPFlowJets_subresjesgscIS_ftf/NoTriggerSelection
	  }
	  hist (et|eta|pt)_(e|eta|m)@expert {
	    regex         = 1			            
            algorithm     = HLTjet_Chi2NDF  
	    description   = AUTOMATIC TEST DISABLED!
	    output        = HLT/TRJET/Expert/Online/HLT_AntiKt4EMPFlowJets_subresjesgscIS_ftf/NoTriggerSelection
	  }
	  # normal th1d with underscores
          hist .*_(central|highmu|forward|LooseBadFailedJets)@expert {
            regex         = 1
            algorithm     = HLTjet_KolmogorovTest_MaxDist
            description   = AUTOMATIC TEST CURRENTLY DISABLED.
            output        = HLT/TRJET/Expert/Online/HLT_AntiKt4EMPFlowJets_subresjesgscIS_ftf/NoTriggerSelection
            display       = StatBox
          }
	  # more normal th1d with underscores
          hist .*Momentum_(pt|eta|m)|@expert {
            regex         = 1
            algorithm     = HLTjet_KolmogorovTest_MaxDist
            description   = AUTOMATIC TEST CURRENTLY DISABLED.
            output        = HLT/TRJET/Expert/Online/HLT_AntiKt4EMPFlowJets_subresjesgscIS_ftf/NoTriggerSelection
            display       = StatBox
          }
	  # more normal th1d with underscores
          hist .*Eta0_32@expert {
            regex         = 1
            algorithm     = HLTjet_KolmogorovTest_MaxDist
            description   = AUTOMATIC TEST CURRENTLY DISABLED.
            output        = HLT/TRJET/Expert/Online/HLT_AntiKt4EMPFlowJets_subresjesgscIS_ftf/NoTriggerSelection
            display       = StatBox
          }
        }
        dir HLT_noalg_L1jJ80 {
          dir ExpertHistos {
            hist .*@expert {
              regex         = 1
              algorithm     = HLTjet_Chi2NDF
              description   = Compare to reference.
              output        = HLT/TRJET/Expert/Online/HLT_AntiKt4EMPFlowJets_subresjesgscIS_ftf/HLT_noalg_L1jJ80
              display       = StatBox
            }
          }
        }
        dir HLT_j60_pf_ftf_preselj50_L1jJ50 {
          dir ExpertHistos {
            hist .*@expert {
              regex         = 1
              algorithm     = HLTjet_Chi2NDF
              description   = Compare to reference.
              output        = HLT/TRJET/Expert/Online/HLT_AntiKt4EMPFlowJets_subresjesgscIS_ftf/HLT_j60_pf_ftf_preselj50_L1jJ50
              display       = StatBox
            }
          }
	  # underscores are expert in chain folders
          hist [^_]*@shifter {
            regex         = 1
            algorithm     = HLTjet_KolmogorovTest_MaxDist
            description   = NO AUTOMATIC CHECKS
            output        = HLT/TRJET/Shifter/Online/HLT_AntiKt4EMPFlowJets_subresjesgscIS_ftf/HLT_j60_pf_ftf_preselj50_L1jJ50
          }
        }
        dir HLT_j45_pf_ftf_preselj20_L1jJ40 {
          dir ExpertHistos {
            hist .*@expert {
              regex         = 1
              algorithm     = HLTjet_Chi2NDF
              description   = NO AUTOMATIC CHECKS
              output        = HLT/TRJET/Expert/Online/HLT_AntiKt4EMPFlowJets_subresjesgscIS_ftf/HLT_j45_pf_ftf_preselj20_L1jJ40
            }
          }
          hist [^_]*@shifter {
            regex         = 1
            algorithm     = HLTjet_KolmogorovTest_MaxDist
            description   = Compare to reference.
            output        = HLT/TRJET/Shifter/Online/HLT_AntiKt4EMPFlowJets_subresjesgscIS_ftf/HLT_j45_pf_ftf_preselj20_L1jJ40
            display       = StatBox
          }
        }
        dir HLT_j45_pf_ftf_preselj20_L1J15 {
          dir ExpertHistos {
            hist .*@expert {
              regex         = 1
              algorithm     = HLTjet_Chi2NDF
              description   = Compare to reference.
              output        = HLT/TRJET/Expert/Online/HLT_AntiKt4EMPFlowJets_subresjesgscIS_ftf/HLT_j45_pf_ftf_preselj20_L1J15
              display       = StatBox
            }
          }
          hist [^_]*@shifter {
            regex         = 1
            algorithm     = HLTjet_KolmogorovTest_MaxDist
            description   = Compare to reference.
            output        = HLT/TRJET/Shifter/Online/HLT_AntiKt4EMPFlowJets_subresjesgscIS_ftf/HLT_j45_pf_ftf_preselj20_L1J15
            display       = StatBox
          }
        }
      }
      dir HLT_AntiKt4EMPFlowJets_subjesgscIS_ftf {
        dir NoTriggerSelection {
          hist .*@expert {
            regex         = 1
            algorithm     = HLTjet_Chi2NDF
            description   = Compare to reference.
            output        = HLT/TRJET/Expert/Online/HLT_AntiKt4EMPFlowJets_subjesgscIS_ftf/NoTriggerSelection
            display       = StatBox
          }
        }
      }
      dir HLT_AntiKt4EMTopoJets_subjesIS {
        dir NoTriggerSelection {
          dir MatchedJets_AntiKt4EMPFlowJets {
            hist [^_]*@shifter {
              regex         = 1
              algorithm     = HLTjet_KolmogorovTest_MaxDist
              description   = Compare to reference.
              output        = HLT/TRJET/Shifter/Online/HLT_AntiKt4EMTopoJets_subjesIS/NoTriggerSelection/MatchedOffline
              display       = StatBox
            }
	    hist .*_.*@shifter {
	      regex         = 1
              algorithm     = HLTjet_Chi2NDF
              description   = Compare to reference.
              output        = HLT/TRJET/Shifter/Online/HLT_AntiKt4EMTopoJets_subjesIS/NoTriggerSelection/MatchedOffline
	    }
          }
	  hist eta_phi@shifter {
            algorithm     = HLTjet_Chi2NDF
            description   = Check for true hotspots (not low-stats artifacts) in EMTopo jet distribution
	    output        = HLT/TRJET/Shifter/Online/HLT_AntiKt4EMTopoJets_subjesIS/NoTriggerSelection
	  }
	  hist phi_e@shifter {
            algorithm     = HLTjet_Chi2NDF
            description   = Check for true hotspots (not low-stats artifacts) in EMTopo jet distribution
	    output        = HLT/TRJET/Shifter/Online/HLT_AntiKt4EMTopoJets_subjesIS/NoTriggerSelection
	  }
	  hist (phi|DetectorEta|pt|e)@shifter {
	    regex         = 1
            algorithm     = HLTjet_KolmogorovTest_MaxDist
            description   = Check for spikes or scale difference from reference in EMTopo jets
            output        = HLT/TRJET/Expert/Online/HLT_AntiKt4EMTopoJets_subjesIS/NoTriggerSelection
            display       = StatBox
          }
          hist .*@expert {
            regex         = 1
            algorithm     = HLTjet_KolmogorovTest_MaxDist
            description   = EMTopo jet checks
            output        = HLT/TRJET/Expert/Online/HLT_AntiKt4EMTopoJets_subjesIS/NoTriggerSelection
            display       = StatBox
          }
        }
        dir HLT_j45_320eta490_L1jJ40p31ETA49 {
          dir ExpertHistos {
            hist .*@expert {
              regex         = 1
              algorithm     = HLTjet_KolmogorovTest_MaxDist
              description   = Check forward jet distributions
              output        = HLT/TRJET/Expert/Online/HLT_AntiKt4EMTopoJets_subjesIS/HLT_j45_320eta490_L1jJ40p31ETA49
            }
          }
          hist [^_]*@shifter {
            regex         = 1
            algorithm     = HLTjet_KolmogorovTest_MaxDist
            description   = Compare to reference.
            output        = HLT/TRJET/Shifter/Online/HLT_AntiKt4EMTopoJets_subjesIS/HLT_j45_320eta490_L1jJ40p31ETA49
            display       = StatBox
          }
        }
        dir HLT_j45_320eta490_L1J15p31ETA49 {
          dir ExpertHistos {
            hist .*@expert {
              regex         = 1
              algorithm     = HLTjet_Chi2NDF
              description   = Compare to reference.
              output        = HLT/TRJET/Expert/Online/HLT_AntiKt4EMTopoJets_subjesIS/HLT_j45_320eta490_L1J15p31ETA49
              display       = StatBox
            }
          }
          hist [^_]*@shifter {
            regex         = 1
            algorithm     = HLTjet_KolmogorovTest_MaxDist
            description   = Compare to reference.
            output        = HLT/TRJET/Shifter/Online/HLT_AntiKt4EMTopoJets_subjesIS/HLT_j45_320eta490_L1J15p31ETA49
            display       = StatBox
          }
        }
      }
    }
    dir Offline {
      description   = Use reco jet response and kinematics to debug issues
      algorithm     = HLTjet_Chi2NDF
      dir AntiKt4EMTopoJets {
        dir standardHistos {
          dir MatchedJets_AntiKt4EMPFlowJets {
            hist .*@expert {
              regex         = 1
              output        = HLT/TRJET/Expert/Offline/AntiKt4EMTopoJets/MatchedPFlowJets
              display       = StatBox
            }
          }
          hist .*@expert {
            regex         = 1
            output        = HLT/TRJET/Expert/Offline/AntiKt4EMTopoJets/standardHistos
            display       = StatBox
          }
        }
        dir LooseBadFailedJets {
          hist [^_]* {
            regex         = 1
            algorithm     = HLTjet_Histogram_Not_Empty_with_Ref&GatherData
            output        = HLT/TRJET/Expert/Offline/AntiKt4EMTopoJets/LooseBadFailedJets
            display       = StatBox
          }
        }
      }
      dir AntiKt4EMPFlowJets {
        dir standardHistos {
          hist [^_]* {
            regex         = 1
            output        = HLT/TRJET/Expert/Offline/AntiKt4EMPFlowJets/standardHistos
            display       = StatBox
          }
        }
        dir LooseBadFailedJets {
          hist [^_]* {
            regex         = 1
            algorithm     = HLTjet_Histogram_Not_Empty_with_Ref&GatherData
            output        = HLT/TRJET/Expert/Offline/AntiKt4EMPFlowJets/LooseBadFailedJets
            display       = StatBox
          }
        }
      }
    }
    dir L1 {
      dir L1_jFexSRJetRoI {
        dir NoTriggerSelection {
          dir MatchedJets_AntiKt4EMPFlowJets {
            hist [^_]* {
              regex         = 1
              algorithm     = HLTjet_Chi2NDF
              description   = Look for changes in jFex response
              output        = HLT/TRJET/Expert/L1/L1_jFexSRJetRoI/NoTriggerSelection/MatchedRecoJets
              display       = StatBox
            }
          }
          dir MatchedJets_HLT_AntiKt4EMPFlowJets_subresjesgscIS_ftf {
            hist [^_]* {
              regex         = 1
              algorithm     = HLTjet_Chi2NDF
              description   = Look for changes in jFex response
              output        = HLT/TRJET/Expert/L1/L1_jFexSRJetRoI/NoTriggerSelection/MatchedTrigJets
              display       = StatBox
            }
          }
          hist .* {
            regex         = 1
            algorithm     = HLTjet_Chi2NDF
            description   = Look for changes in jFex RoIs
            output        = HLT/TRJET/Shifter/L1/L1_jFexSRJetRoI
            display       = StatBox
          }
        }
        dir L1_jJ40 {
          hist [^_]* {
            regex         = 1
            algorithm     = HLTjet_Chi2NDF
            description   = Check threshold and look for changes in jFex RoIs
            output        = HLT/TRJET/Expert/L1/L1_jFexSRJetRoI/L1_jJ40
            display       = StatBox
          }
        }
        dir L1_jJ50 {
          hist [^_]* {
            regex         = 1
            algorithm     = HLTjet_Chi2NDF
            description   = Check threshold and look for changes in jFex RoIs
            output        = HLT/TRJET/Expert/L1/L1_jFexSRJetRoI/L1_jJ50
            display       = StatBox
          }
        }
      }
      dir LVL1JetRoIs {
        dir NoTriggerSelection {
          dir MatchedJets_AntiKt4EMPFlowJets {
            hist [^_]* {
              regex         = 1
              algorithm     = HLTjet_Histogram_Not_Empty_with_Ref&GatherData
              description   = Check for	change in response
              output        = HLT/TRJET/Expert/L1/LVL1JetRoIs/NoTriggerSelection/MatchedRecoJets
              display       = StatBox
            }
          }
          dir MatchedJets_HLT_AntiKt4EMPFlowJets_subresjesgscIS_ftf {
            hist [^_]* {
              regex         = 1
              algorithm     = HLTjet_Histogram_Not_Empty_with_Ref&GatherData
              description   = Check for change in response
              output        = HLT/TRJET/Expert/L1/LVL1JetRoIs/NoTriggerSelection/MatchedTrigJets
              display       = StatBox
            }
          }
          hist .* {
            regex         = 1
            algorithm     = HLTjet_Chi2NDF
            description   = Compare to reference
            output        = HLT/TRJET/Shifter/L1/LVL1JetRoIs
            display       = StatBox
          }
        }
        dir L1_J20 {
          hist [^_]* {
            regex         = 1
            algorithm     = HLTjet_Chi2NDF
            description   = Check threshold and compare to reference.
            output        = HLT/TRJET/Expert/L1/LVL1JetRoIs/L1_J20
            display       = StatBox
          }
        }
        dir L1_J15 {
          hist [^_]* {
            regex         = 1
            algorithm     = HLTjet_Chi2NDF
            description   = Check threshold and compare to reference.
            output        = HLT/TRJET/Expert/L1/LVL1JetRoIs/L1_J15
            display       = StatBox
          }
        }
      }
    }
  }
}

